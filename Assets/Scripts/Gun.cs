using UnityEngine;

public class Gun : Weapon
{
    protected Vector2 startPosition;
    private float squareLifeDistance;
    private Rigidbody2D riddick;

    public override void Initialize(Transform targetTransform)
    {
        riddick = GetComponent<Rigidbody2D>();
        squareLifeDistance = LifeDistance * LifeDistance;
        startPosition = transform.position;
        Vector2 myPos = new Vector2(transform.position.x, transform.position.y);
        Vector2 target = new Vector2(targetTransform.position.x, targetTransform.position.y);
        Vector2 direction = (target - myPos).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        riddick.velocity = direction * ProjectileSpeed;
        isInitialized = true;
    }

    protected void Update()
    {
        if (!isInitialized)
            return;

        if ((new Vector2(transform.position.x, transform.position.y) - startPosition).SqrMagnitude() >= squareLifeDistance)
        {
            Die();
        }

    }
}
