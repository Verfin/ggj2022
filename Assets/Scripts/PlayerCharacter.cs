using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public enum CharacterType
{
    Player1,
    Player2,
    Boss
}

public class PlayerCharacter : NetworkBehaviour
{
    public static PlayerCharacter GetMyPlayer()
    {
        foreach (var item in FindObjectsOfType<PlayerCharacter>())
        {
            if (item.isLocalPlayer)
                return item;
        }

        return null;
    }

    Vector2 lastInput;

    Vector2 startScale;

    public float MaxHealth = 100;
    public float DamageModifier = 1;
    public float SpeedModifier = 1;
    public float AttackSpeedModifier = 1;

    public Transform HealthBar;

    [SyncVar]
    public float Health = 100;
    [SyncVar]
    public CharacterType CharacterType;
    [SyncVar]
    public float PlayerSpeed = 3;
    [SyncVar]
    public bool isAlive = true;
    Rigidbody2D rigidbody2d;
    public GameObject cameraGo;
    public AudioClip HitSound;
    public SpriteRenderer sr;

    public LayerMask layer;
    public bool CanMove = true;

    float blinkTimer = 0;

    private List<Enemy> enemiesInsideMe = new List<Enemy>();
    [SyncVar]
    private int enemyCountInsideMe = 0;

    private List<Buff> activeBuffs = new List<Buff>();

    protected virtual void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        isAlive = true;
        rigidbody2d = GetComponent<Rigidbody2D>();
        if (isLocalPlayer)
        {
            GameObject go = Instantiate(cameraGo);
            go.transform.SetParent(transform);
            go.transform.localPosition = new Vector3(0,0,-10);
            // Ignore stuff on other player's layer 
            string playerLayer = CharacterType == CharacterType.Player1 ? "player2" : "player1";
            go.GetComponent<Camera>().cullingMask = ~(1 << LayerMask.NameToLayer(playerLayer));
        }
        Health = MaxHealth;
    }

    public void ShowLevelUp(int weapon1, int weapon2)
    {
        RpcShowLevelUp(weapon1, weapon2);
    }

    public void SelectWeapon(CharacterType characterType, int weapon)
    {
        CmdSelectWeapon(characterType, weapon);
    }

    [Command]
    public void CmdSelectWeapon(CharacterType characterType, int weapon)
    {
        ExpManager.Instance.CmdSelectWeapon(characterType, weapon);
    }

     [ClientRpc]
    public void RpcShowLevelUp(int weapon1, int weapon2)
    {
        if (!isLocalPlayer)
            return;
        ExpManager.Instance.ActuallyShowLevelUpScreen(weapon1, weapon2);
    }

    private void OnDrawGizmos()
    {
        //    Gizmos.DrawLine(gizmoFrom.Value, gizmoTo.Value);
    }

    [Command]
    void CmdUpdateAxisOnServer(float horizontalInput, float verticalInput)
    {
        //TestDebugger.PrintFunction();
        if (!isAlive)
        {
            lastInput = Vector2.zero;
            horizontalInput = 0;
            verticalInput = 0;
            return;
        }
        lastInput = new Vector2(horizontalInput, verticalInput);
        if (horizontalInput != 0 || verticalInput != 0)
        {
            lastInput = lastInput.normalized;
        }
    }

    void FixedUpdate()
    {
        if (isServer)
        {
            // cap movement speed to 3x normal to prevent player from running outside of arena
            float speedBuffModifier = GetBuffStrength(BuffType.Speed) > 3 ? 3 : GetBuffStrength(BuffType.Speed);
            rigidbody2d.MovePosition(transform.position + new Vector3(lastInput.x, lastInput.y, 0) * PlayerSpeed * speedBuffModifier * Time.fixedDeltaTime);
        }
    }

    public override void OnStartLocalPlayer()
    {
        //GetComponent<SpriteRenderer>().material.color = Color.red; debuggitarkotuksiin
    }

    protected virtual void Update()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 0)
            return;

        HealthBar.GetChild(0).transform.localScale = new Vector3(Health / MaxHealth, 1, 1);
        if (sr == null)
        {
            sr = GetComponentInChildren<SpriteRenderer>();
        }
        if (sr.sprite != null)
        {
            float ZOffset = sr.sprite.bounds.size.y * -0.5f;
            //transform.position = SpriteUtilities.GetSpriteLocation(transform, ZOffset);
        }
        if (isServer)
        {
            enemyCountInsideMe = enemiesInsideMe.Count;
            for (int i = enemyCountInsideMe-1; i >= 0; i--)
            {
                if (!enemiesInsideMe[i].IsAlive)
                {
                    enemiesInsideMe.RemoveAt(i);
                }
            }
            float damage = 0;
            foreach (Enemy enemy in enemiesInsideMe)
            {
                if (enemy.TargetPlayerType == CharacterType)
                damage += enemy.Damage;
            }
            if (damage > 0)
            {
                Hit(damage * Time.deltaTime);
            }

            UpdateBuffs();
            // GGJ 2022 TODO: Handle weapon cooldowns, shooting etc..
        }

        if (enemyCountInsideMe > 0)
        {
            AudioManager.Instance.PlaySound(HitSound);
            blinkTimer += Time.deltaTime * 10;
            BlinkSprite();
        } else
        {
            blinkTimer = 0;
            sr.color = Color.white;
        }

        if (!isLocalPlayer || !isAlive || !CanMove)
            return;
      
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");
        CmdUpdateAxisOnServer(horizontalInput, verticalInput);
    }

    void Hit(float damage)
    {
        if (!isServer)
        {
            return;
        }
        Health -= damage;
        if (Health <= 0)
        {
            Health = 0;
            HealthBar.gameObject.SetActive(false);
            Die();
        }
    }

    void BlinkSprite()
    {
        float c = Mathf.Sin(blinkTimer);
        sr.color = new Color(1, c, c);
    }

    void Die()
    {
        if (!isServer)
        {
            return;
        }
        GetComponent<Animator>().SetBool("alive", false);
        GetComponentsInChildren<Shooter>().ToList().ForEach(x => x.enabled = false);
        GetComponentsInChildren<RotatingShooter>().ToList().ForEach(x => x.enabled = false);
        isAlive = false;
        Time.timeScale = 0;
        FindObjectOfType<GameOver>().Show();
        // game over
    }

    public void AddBuff(Buff buff)
    {
        if (buff.Type == BuffType.Health)
        {
            Heal(buff.Strength);
        }
        else
        {
            activeBuffs.Add(buff);
        }
    }

    private void Heal(float strength)
    {
        Health += MaxHealth * strength;
        if (Health > MaxHealth)
        {
            Health = MaxHealth;
        }
        HealthBar.GetChild(0).transform.localScale = new Vector3(Health / MaxHealth, 1, 1);
    }

    public float GetBuffStrength(BuffType bt)
    {
        float strength = 1;
        foreach (Buff buff in activeBuffs)
        {
            if (buff.Type == bt)
            {
                strength *= buff.Strength;
            }
        }
        return strength;
    }

    private void UpdateBuffs()
    {
        if (activeBuffs.Count == 0)
            return;
        for (int i = activeBuffs.Count-1; i >= 0; i--)
        {
            Buff b = activeBuffs[i];
            b.Duration -= Time.deltaTime;
            if (b.Duration <= 0)
            {
                activeBuffs.RemoveAt(i);
                continue;
            }
            activeBuffs[i] = b;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        var gamo = col.gameObject;
        var enemy = gamo.GetComponent<Enemy>();
        if (enemy != null && enemy.TargetPlayerType == CharacterType)
        {
            if (isServer && !enemiesInsideMe.Contains(enemy))
            {
                enemiesInsideMe.Add(enemy);
            }
        }
        Collectible collectible = gamo.GetComponent<Collectible>();
        if (collectible != null)
        {
            collectible.OnCollect(this);
        }

        var weapon = gamo.GetComponent<Weapon>();
        if (weapon?.ShooterCharacterType == CharacterType.Boss)
        {
            Hit(weapon.Damage);
            weapon.Die();
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        var hit = col.gameObject;
        var enemy = hit.GetComponent<Enemy>();
        if (enemy != null)
        {
            if (isServer)
            {
                enemiesInsideMe.Remove(enemy);
            }
        }
    }
}
