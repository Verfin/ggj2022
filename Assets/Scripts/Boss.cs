using UnityEngine;
using UnityEngine.Networking;

public class Boss : NetworkBehaviour
{
    public float MaxHealth;
    [SyncVar]
    public bool IsAlive = true;
    public float Damage;
    [SyncVar]
    public float Speed;
    [SyncVar]
    public float Health;
    public Transform HealthBar;
    public AudioClip DeathAudio;

    private PlayerCharacter targetPlayer;
    private Rigidbody2D rigidbody2d;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        HealthBar.GetChild(0).transform.localScale = new Vector3(Health / MaxHealth, 1, 1);

    }

    void FixedUpdate()
    {
        Vector2 direction;
        if (!IsAlive)
            return;

        if (!targetPlayer)
        {
            targetPlayer = GetClosestPlayer();
        }
        if (targetPlayer && isServer)
        {
            direction = (targetPlayer.transform.position - transform.position).normalized;
            rigidbody2d.MovePosition(transform.position + new Vector3(direction.x, direction.y, 0) * Speed * Time.fixedDeltaTime);
        }
        else if (!targetPlayer)
        {
            Debug.LogWarning("No targetplayer found!1!11");
        }
    }

    public void Hit(float damage)
    {
        Health = Health - damage;
        if (Health <= 0)
        {
            Health = 0;
            if (IsAlive)
            {
                HealthBar.gameObject.SetActive(false);
                RpcDie();
                CollectibleManager.Instance.GenerateRandomPowerup(transform.position);
                FindObjectOfType<EnemySpawnManager>().OnEnemyDied(gameObject);
            }
        }
    }

    [ClientRpc]
    private void RpcDie()
    {
        foreach (Collider2D col in GetComponents<Collider2D>())
            col.enabled = false;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //GetComponent<Animator>().SetBool("alive", false);
        // death anim
        // whatever
        // todo: do we need to destroy the enemy on network server....?
        AudioManager.Instance.PlaySound(DeathAudio);
        IsAlive = false;
        EnemySpawnManager.Instance.IncreaseKillCount();
        Destroy(gameObject, 1f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isServer)
            return;
        Weapon bullet = collision.GetComponent<Weapon>();
        if (bullet != null)
        {
            bool killBullet = bullet.ShooterCharacterType != CharacterType.Boss;
            bool hits = bullet.ShooterCharacterType != CharacterType.Boss;
            if (hits)
            {
                Hit(bullet.Damage);
            }

            if (killBullet && bullet.DiesOnHit)
            {
                bullet.Die();
            }
        }
    }

    private PlayerCharacter GetClosestPlayer()
    {
        var enemies = FindObjectsOfType<PlayerCharacter>();

        PlayerCharacter bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (PlayerCharacter potentialTarget in enemies)
        {

            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }
}
