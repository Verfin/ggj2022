using UnityEngine;

public class RandomBullent : Weapon
{
    private Rigidbody2D riddick;
    protected Vector2 startPosition;
    private float squareLifeDistance;


    public override void Initialize(Transform targetTransform)
    {
        riddick = GetComponent<Rigidbody2D>();
        squareLifeDistance = LifeDistance * LifeDistance;
        startPosition = transform.position;
        var x = Random.Range(-1f, 1f);
        var y = Random.Range(-1f, 1f);
        var direction = new Vector3(x, y, 0);
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
        riddick.velocity = direction * ProjectileSpeed;
        isInitialized = true;
    }

    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!isInitialized)
            return;

        if ((new Vector2(transform.position.x, transform.position.y) - startPosition).SqrMagnitude() >= squareLifeDistance)
        {
            Die();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Enemy>() != null)
            Die();
    }
}
