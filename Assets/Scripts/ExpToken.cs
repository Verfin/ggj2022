using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum ExpType
{ 
    Small,
    Medium,
    Huge,
}

public class ExpToken : Collectible
{
    public static ExpToken InstantiateExp(Vector3 location, ExpType type)
    {
        GameObject go = new GameObject("Exp Token",new System.Type[] { typeof(Transform), typeof(ExpToken) });
        ExpToken exp = go.GetComponent<ExpToken>();
        exp.type = type;
        return exp;
    }

    public ExpType type;

    protected override void Collect(PlayerCharacter pc)
    {
        float amount = type switch
        {
            ExpType.Small => 5,
            ExpType.Medium => 10,
            ExpType.Huge => 20,
            _ => throw new System.NotImplementedException()
        };

        ExpManager.Instance.CollectExp(amount);
    }
}
