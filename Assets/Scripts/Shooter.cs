using UnityEngine;
using UnityEngine.Networking;

public class Shooter : NetworkBehaviour
{
    private PlayerCharacter playerCharacter;
    private float attackTimer;
    public GameObject Weapon;

    // Start is called before the first frame update
    void Start()
    {
        attackTimer = Weapon.GetComponent<Weapon>().AttackSpeed;
        playerCharacter = GetComponentInParent<PlayerCharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        attackTimer -= Time.deltaTime * playerCharacter.GetBuffStrength(BuffType.Speed);
        if (attackTimer <= 0)
        {
            for (var i = 0; i < Weapon.GetComponent<Weapon>().NumberOfProjectiles; i++)
            {
                var targetTransform = GetClosestEnemy();
                if (targetTransform != null)
                {
                    var go = Instantiate(Weapon, transform.position, transform.rotation);
                    go.GetComponent<Weapon>().Damage *= playerCharacter.GetBuffStrength(BuffType.Damage);
                    go.GetComponent<Weapon>().Initialize(targetTransform);
                    go.GetComponent<Weapon>().SetShooter(playerCharacter.CharacterType);
                    NetworkServer.Spawn(go);
                }
            }
            attackTimer = Weapon.GetComponent<Weapon>().AttackSpeed;
        }
    }

    private Transform GetClosestEnemy()
    {
        var enemies = FindObjectsOfType<Enemy>();

        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Enemy potentialTarget in enemies)
        {
            if (potentialTarget.TargetPlayerType == playerCharacter.CharacterType || !potentialTarget.IsAlive)
                continue;

            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget.transform;
            }
        }

        var bosses = FindObjectsOfType<Boss>();
        foreach (Boss potentialTarget in bosses)
        {
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget.transform;
            }
        }

        return bestTarget;
    }
}
