using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BossShooter : NetworkBehaviour
{
    private float attackTimer;
    public GameObject Weapon;
    public float Damage;

    // Start is called before the first frame update
    void Awake()
    {
        attackTimer = Weapon.GetComponent<Weapon>().AttackSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (isServer)
        {
            attackTimer -= Time.deltaTime;
            if (attackTimer <= 0)
            {
                for (var i = 0; i < Weapon.GetComponent<Weapon>().NumberOfProjectiles; i++)
                {
                    var targetTransform = GetClosestPlayer();
                    if (targetTransform != null)
                    {
                        var go = Instantiate(Weapon, transform.position, transform.rotation);
                        go.GetComponent<Weapon>().Damage *= Damage;
                        go.GetComponent<Weapon>().SetShooter(CharacterType.Boss);
                        NetworkServer.Spawn(go);
                        go.GetComponent<Weapon>().Initialize(targetTransform);
                    }
                }
                attackTimer = Weapon.GetComponent<Weapon>().AttackSpeed;
            }
        }
    }

    private Transform GetClosestPlayer()
    {
        var enemies = FindObjectsOfType<PlayerCharacter>();

        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (PlayerCharacter potentialTarget in enemies)
        {
            if (!potentialTarget.isAlive)
                continue;

            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget.transform;
            }
        }

        return bestTarget;
    }
}
