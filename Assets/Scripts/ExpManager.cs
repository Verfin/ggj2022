using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public delegate void ExpDelegate(ExpManager mgr, int weapon1, int weapon2);

[System.Serializable]
public struct UnityNeedsHelp
{
    public ShooterType Type;
    public GameObject Prefab;
}

public class ExpManager : NetworkBehaviour
{
    public static ExpManager Instance;
    public List<GameObject> WeaponPrefabs;
    public List<UnityNeedsHelp> ShooterPrefabs;
    bool singlePlayer = false;

    [SyncVar]
    public float RemainingExp = 20;
    [SyncVar]
    public float MaxExp = 20;
    [SyncVar]
    public bool HasLevel = false;

    public Text ExpText;

    // server only
    public int? Player1Ready = null;
    public int? Player2Ready = null;

    public event ExpDelegate OnShowLevelScreen;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if (isServer)
            StartCoroutine(LevelUp());
    }

    public void CollectExp(float exp)
    {
        if (!isServer || HasLevel)
            return;

        RemainingExp -= exp;
        
        if (RemainingExp <= 0)
        {
            StartCoroutine(LevelUp());
        }
    }

    public IEnumerator LevelUp()
    {
        HasLevel = true;
        yield return new WaitForSeconds(1);

        if (!isServer)
            yield break;

        MaxExp = MaxExp * 1.2f + 10;
        RemainingExp = MaxExp;
        Time.timeScale = 0;
        PlayerCharacter[] players = FindObjectsOfType<PlayerCharacter>();
        singlePlayer = players.Length == 1;
        foreach (PlayerCharacter pc in players)
        {
            int rng1 = Random.Range(0, WeaponPrefabs.Count);
            int rng2 = Random.Range(0, WeaponPrefabs.Count);
            int breaker = 99;
            while (rng2 == rng1)
            {
                rng2 = Random.Range(0, WeaponPrefabs.Count);
                breaker--;
                if (breaker <= 0)
                {
                    Debug.LogError("YIKES");
                    break;
                }
            }

            pc.ShowLevelUp(rng1, rng2);
        }
    }

    public void ActuallyShowLevelUpScreen(int weapon1, int weapon2)
    {
        OnShowLevelScreen?.Invoke(this, weapon1, weapon2);
    }

    [Command]
    public void CmdSelectWeapon(CharacterType characterType, int weapon)
    {
        var players = FindObjectsOfType<PlayerCharacter>();
        GameObject wep = WeaponPrefabs[weapon];
        if (wep == null)
            Debug.LogError("NOOO");
        ShooterType st = wep.GetComponent<Weapon>().ShooterType;
        foreach (var p in players)
        {
            if (p.CharacterType == characterType)
            {
                GameObject shooter = Instantiate(ShooterPrefabs.Find(x => x.Type == st).Prefab, p.gameObject.transform);
                shooter.transform.localPosition = Vector3.zero;
                if (st == ShooterType.Straight)
                    shooter.GetComponent<Shooter>().Weapon = wep;
                else if (st == ShooterType.Circle)
                    shooter.GetComponent<RotatingShooter>().Weapon = wep;
            }
        }

        if (characterType == CharacterType.Player1)
        {
            Player1Ready = weapon;

        }
        else
        {
            Player2Ready = weapon;
        }
    }

    private void Update()
    {
        if (RemainingExp < 0)
            RemainingExp = 0;
        if (ExpText != null)
            ExpText.text = "Experience: " + (MaxExp - RemainingExp) + " / " + MaxExp;
        if (HasLevel && Player1Ready != null && (Player2Ready != null || singlePlayer))
        {
            HasLevel = false;
            Player1Ready = null;
            Player2Ready = null;
            Time.timeScale = 1;
        }
    }
}
