﻿using UnityEngine;

public abstract class SingletonBase<T> : MonoBehaviour where T : SingletonBase<T>
{
    public static T Instance
    #region SingletonPattern
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<T>();
                if (instance == null || instance.Equals(null)) // check if the instance is actually null, then check if the object is being destroyed
                {
                    instance = new GameObject(typeof(T).Name).AddComponent<T>();
                }
            }

            return instance;
        }
    }

    private static T instance;
    #endregion

    public static void DestroyInstance()
    {
        instance.DeInitialize();
        DestroyImmediate(instance);
        instance = null;
    }

    protected bool initialized;
    protected virtual void Awake()
    {
        if (instance == null)
        {
            instance = this as T;
        }
        else if (instance != this)
        {
            if (gameObject.GetComponents<Component>().Length > 1)
            {
                GameObject.Destroy(this);
            }
            else
            {
                GameObject.Destroy(this.gameObject);
            }

            return;
        }

        GameObject.DontDestroyOnLoad(this.gameObject);
    }

    public virtual bool Initialize()
    {
        if (initialized)
        {
            return false;
        }

        initialized = true;
        return true;
    }

    public bool Initialized { get { return initialized; } }

    protected abstract void DeInitialize();
}
