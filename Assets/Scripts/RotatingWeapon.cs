using UnityEngine;

public class RotatingWeapon : Weapon
{
    private Transform target;
    public float DistanceFromPlayer;

    public override void Initialize(Transform targetTransform)
    {
        target = targetTransform;
        transform.localPosition = new Vector3(DistanceFromPlayer, 0, 0);
        isInitialized = true;
    }

    private Vector3 zAxis = new Vector3(0, 0, 1);

    void FixedUpdate()
    {
        if (!isInitialized)
            return;

        transform.RotateAround(target.position + new Vector3(0, 0, 0), zAxis, ProjectileSpeed);
    }
}
