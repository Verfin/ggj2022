using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NovaWeapon : Weapon
{
    float size;

    public override void Initialize(Transform targetTransform)
    {

    }

    private void Update()
    {
        size += Time.deltaTime * ProjectileSpeed;
        transform.localScale = Vector3.one * size;
        if (size > LifeDistance)
        {
            Die();
        }
    }
}
