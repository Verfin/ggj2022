using UnityEngine;
using UnityEngine.Networking;

public enum ShooterType
{
    Straight,
    Circle,
}

public abstract class Weapon : NetworkBehaviour
{
    public float ProjectileSpeed;
    public float AttackSpeed;
    public float Damage;
    public bool DiesOnHit;
    public float LifeDistance;
    public int NumberOfProjectiles;
    public Sprite DisplayIcon;
    public string DisplayName;
    public string Description;
    public ShooterType ShooterType;
    public RotatingShooter Parent;
    public BossRotatingShooter BossParent;
    public AudioClip OnHitSound;
    public bool HitsWrongEnemies = false;

    public CharacterType ShooterCharacterType;
    protected bool isInitialized = false;

    public abstract void Initialize(Transform targetTransform);

    public bool IsShooterPlayer => ShooterCharacterType == CharacterType.Player1 || ShooterCharacterType == CharacterType.Player2;


    public void SetShooter(CharacterType shooter)
    {
        ShooterCharacterType = shooter;
    }
    public void Die()
    {
        if (Parent != null)
            Parent.BulletDestroyed();
        if (BossParent != null)
            BossParent.BulletDestroyed();

        AudioManager.Instance.PlaySound(OnHitSound);
        NetworkServer.Destroy(gameObject);
        Destroy(gameObject);
    }
}
