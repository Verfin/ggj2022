using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public struct EnemyWave
{
    public EnemyWave(float time, float points)
    {
        TimeLeft = time;
        SpawnPoints = points;
    }

    public float TimeLeft;
    public float SpawnPoints;
}

public class EnemySpawnManager : UnityEngine.Networking.NetworkBehaviour
{
    public List<GameObject> EnemyPrefabs;
    public List<GameObject> BossPrefabs;
    public int KillCount = 0;
    public int KillsRequiredForBoss;
    public int KillsUntilNextBossCounter;
    private Queue<EnemyWave> WaveQueue = new Queue<EnemyWave>();
    private List<GameObject> AliveEnemies = new List<GameObject>();
    private List<Vector3> SpawnPoints = new List<Vector3>();
    private PlayerCharacter[] players = null;
    private int WaveCount = 1;
    private float exponent = 1;
    private EnemyWave? CurrentWave;

    int initialEasyMode = 3;

    public static EnemySpawnManager Instance;

    void Awake()
    {
        Instance = this;
        KillsUntilNextBossCounter = KillsRequiredForBoss;
    }

    public void IncreaseKillCount()
    {
        KillCount++;
        KillsUntilNextBossCounter--;
    }

    public void OnEnemyDied(GameObject enemy)
    {
        AliveEnemies.Remove(enemy);
    }

    public void RegisterSpawnPoint(Transform spawnPoint)
    {
        SpawnPoints.Add(spawnPoint.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isServer)
            return;

        if (WaveQueue.Count == 0)
            GenerateQueue();

        if (!CurrentWave.HasValue)
            CurrentWave = WaveQueue.Dequeue();

        if (KillsUntilNextBossCounter <= 0)
        {
            SpawnBoss();
        }
        else if (CurrentWave.Value.TimeLeft <= 0)
        {
            SpawnWave(CurrentWave.Value);
            CurrentWave = null;
        }
        else
        {
            EnemyWave wave = new EnemyWave(CurrentWave.Value.TimeLeft - Time.deltaTime, CurrentWave.Value.SpawnPoints);
            CurrentWave = wave;
        }

        UpdateSpawnLocations();
    }

    void GenerateQueue()
    {
        float spawnMulti = Mathf.Clamp01(1f - (AliveEnemies.Count / 100f)) / 2f + 0.5f;
        int amount = Random.Range(3, 6);
        float time = 12f - 3f * initialEasyMode;
        for (int i = 0; i < amount; i++)
        {
            float rng = (Random.value / 2f + 0.5f);
            float Points = Mathf.Max((WaveCount * 1.2f) - initialEasyMode, 1) * spawnMulti * rng + 2f;
            Points = Mathf.Pow(Points, exponent);
            Debug.Log(string.Format("Queued wave with {0} points", Points));
            WaveQueue.Enqueue(new EnemyWave(time, Points));
            time = Random.Range(4f, 7f);
        }

        WaveCount++;
        exponent += 0.1f;
        if (initialEasyMode > 0)
            initialEasyMode--;
    }

    void SpawnWave(EnemyWave wave)
    {
        float points = wave.SpawnPoints;
        int breaker = 99;

        bool sameUnit = Random.value > 0.5;

        if (sameUnit)
        {
            int idx = Random.Range(0, EnemyPrefabs.Count);
            Enemy e = SpawnEnemy(idx, GetRandomSpawnPointIndex());
            int cost = e.SpawnCost;
            if (cost <= 0)
            {
                cost = 1;
            }

            int amount = Mathf.CeilToInt(points / cost) - 1;
            for (int i = 0; i < amount; i++)
            {
                SpawnEnemy(idx, GetRandomSpawnPointIndex());
            }
        }
        else
        {
            while (points > 0)
            {
                int idx = Random.Range(0, EnemyPrefabs.Count);
                Enemy e = SpawnEnemy(idx, GetRandomSpawnPointIndex());
                points -= e.SpawnCost;
                breaker--;
                if (breaker <= 0)
                {
                    Debug.LogError("OH NO");
                    break;
                }
            }
        }
    }

    Enemy SpawnEnemy(int index, int spawnPointIdx)
    {
        Vector3 spawnLocation = SpawnPoints[spawnPointIdx];
        GameObject go = GameObject.Instantiate(EnemyPrefabs[index], (Vector3)MathfExtensions.RandomPointInCircle(3) + spawnLocation, Quaternion.identity);
        Enemy e = go.GetComponent<Enemy>();
        e.TargetPlayerType = CharacterType.Player1;
        UnityEngine.Networking.NetworkServer.Spawn(go);
        AliveEnemies.Add(go);

        go = GameObject.Instantiate(EnemyPrefabs[index], (Vector3)MathfExtensions.RandomPointInCircle(3) + spawnLocation, Quaternion.identity);
        e = go.GetComponent<Enemy>();
        e.TargetPlayerType = CharacterType.Player2;
        UnityEngine.Networking.NetworkServer.Spawn(go);
        AliveEnemies.Add(go);
        return e;
    }

    void SpawnBoss()
    {
        GameObject go = GameObject.Instantiate(BossPrefabs[Random.Range(0, BossPrefabs.Count)], (Vector3)(MathfExtensions.RandomPointInCircle(3)) + SpawnPoints[GetRandomSpawnPointIndex()], Quaternion.identity);
        UnityEngine.Networking.NetworkServer.Spawn(go);
        KillsRequiredForBoss = Mathf.RoundToInt(KillsRequiredForBoss * 1.2f);
        KillsUntilNextBossCounter = KillsRequiredForBoss;
    }

    int GetRandomSpawnPointIndex()
    {
        int idx = 0;
        float rng = Random.value;

        for (; idx < SpawnPoints.Count; idx++)
        {
            rng -= 1f / (2f + idx);
            if (rng <= 0)
                return idx;
        }

        return SpawnPoints.Count - 1;
    }

    void UpdateSpawnLocations()
    {
        if (players == null)
            players = FindObjectsOfType<PlayerCharacter>();

        List<Vector3> playerLocations = new List<Vector3>();
        foreach (PlayerCharacter pc in players)
        {
            playerLocations.Add(pc.gameObject.transform.position);
        }

        SpawnPoints.Sort((Vector3 a, Vector3 b) =>
        {
            float distA = 99999999;
            float distB = 99999999;
            foreach (Vector3 v in playerLocations)
            {
                float dist = (v - a).sqrMagnitude;
                if (dist < distA && dist > 8)
                {
                    distA = dist;
                }

                dist = (v - b).sqrMagnitude;
                if (dist < distB && dist > 8)
                {
                    distB = dist;
                }
            }

            return distA.CompareTo(distB);
        });
    }
}
