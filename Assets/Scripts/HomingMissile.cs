using UnityEngine;

public class HomingMissile : Weapon
{
    public Transform target = null;
    public Rigidbody2D rigidBody;
    public float angleChangingSpeed;

    public override void Initialize(Transform targetTransform)
    {
        target = targetTransform;
        rigidBody = GetComponent<Rigidbody2D>();
        Vector2 targetDirection = new Vector2(targetTransform.position.x, targetTransform.position.y);
        Vector2 myPos = new Vector2(transform.position.x, transform.position.y);
        Vector2 direction = targetDirection - myPos;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        rigidBody.velocity = direction * ProjectileSpeed;
        isInitialized = true;
    }

    void FixedUpdate()
    {
        if (!isInitialized || target == null)
            return;
        
        Vector2 direction = (Vector2)target.position - rigidBody.position;
        direction.Normalize();
        float rotateAmount = Vector3.Cross(direction, transform.up).z;
        rigidBody.angularVelocity = -angleChangingSpeed * rotateAmount;
        rigidBody.velocity = transform.up * ProjectileSpeed;
    }
}