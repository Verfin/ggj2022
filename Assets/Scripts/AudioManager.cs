using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct CooldownStruct
{
    public AudioClip Clip;
    public float Cooldown;

    public CooldownStruct(AudioClip clip, float cooldown)
    {
        Clip = clip;
        Cooldown = cooldown;
    }
}

public class AudioManager : SingletonBase<AudioManager>
{
    List<AudioSource> activeAudioSources = new List<AudioSource>();
    List<AudioSource> inactiveAudioSources = new List<AudioSource>();
    List<CooldownStruct> cooldownList = new List<CooldownStruct>();

    private void Update()
    {
        for (int i = activeAudioSources.Count - 1; i >= 0; i--)
        {
            AudioSource source = activeAudioSources[i];
            if (!source.isPlaying)
            {
                activeAudioSources.RemoveAt(i);
                inactiveAudioSources.Add(source);
            }
        }

        for (int i = cooldownList.Count - 1; i >= 0; i--)
        {
            CooldownStruct cs = cooldownList[i];
            cs.Cooldown -= Time.unscaledDeltaTime;
            if (cs.Cooldown < 0)
            {
                cooldownList.RemoveAt(i);
            }
            else
            {
                cooldownList[i] = cs;
            }
        }
    }

    public void PlaySound(AudioClip clip)
    {
        if (clip == null)
            return;

        foreach (CooldownStruct cs in cooldownList)
        {
            if (cs.Clip == clip)
            {
                return;
            }
        }

        AudioSource source = null;
        if (inactiveAudioSources.Count > 0)
        {
            source = inactiveAudioSources[inactiveAudioSources.Count - 1];
            inactiveAudioSources.RemoveAt(inactiveAudioSources.Count - 1);
        }
        else
        {
            source = gameObject.AddComponent<AudioSource>();
        }

        activeAudioSources.Add(source);
        source.PlayOneShot(clip);
        cooldownList.Add(new CooldownStruct(clip, clip.length * 0.8f));
    }

    protected override void DeInitialize() { }
}
