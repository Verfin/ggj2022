using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStatsController : MonoBehaviour
{
    public Slider ExpSlider;
    public Text KillCount;
    public LevelUpScreenController LevelUp;

    // Start is called before the first frame update
    void Start()
    {
        ExpManager exp = ExpManager.Instance;
        exp.OnShowLevelScreen += Exp_OnShowLevelScreen;
    }

    private void Exp_OnShowLevelScreen(ExpManager mgr, int weapon1, int weapon2)
    {
        LevelUp.ShowLevelUp(weapon1, weapon2);
    }

    // Update is called once per frame
    void Update()
    {
        ExpManager exp = ExpManager.Instance;
        float expPercent = 1 - (exp.RemainingExp / exp.MaxExp);
        ExpSlider.value = expPercent;
        KillCount.text = EnemySpawnManager.Instance.KillCount.ToString();
    }
}
