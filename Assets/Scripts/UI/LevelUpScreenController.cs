using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public struct LevelUpWeapon
{
    public int PrefabIndex;
    public Sprite Icon;
    public string Name;
    public string Description;

    public LevelUpWeapon(int prefabIndex, Sprite icon, string name, string description)
    {
        PrefabIndex = prefabIndex;
        Icon = icon;
        Name = name;
        Description = description;
    }
}

public class LevelUpScreenController : MonoBehaviour
{
    public GameObject WeaponSelectionWidgetPrefab;
    public RectTransform WeaponSelectionPanel;
    public CanvasGroup LevelUpScreenCanvasGroup;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowLevelUp(int weapon1, int weapon2)
    {
        for (int i = 0; i < WeaponSelectionPanel.childCount; i++)
        {
            Destroy(WeaponSelectionPanel.GetChild(i));
        }

        WeaponSelectionPanel.DetachChildren();

        Weapon wep1 = ExpManager.Instance.WeaponPrefabs[weapon1].GetComponent<Weapon>();
        Weapon wep2 = ExpManager.Instance.WeaponPrefabs[weapon2].GetComponent<Weapon>();

        // TODO: wapon1 and 2 should be a list if possible..
        List<LevelUpWeapon> WeaponOptions = new List<LevelUpWeapon>();
        WeaponOptions.Add(new LevelUpWeapon(weapon1, wep1.DisplayIcon, wep1.DisplayName, wep1.Description));
        WeaponOptions.Add(new LevelUpWeapon(weapon2, wep2.DisplayIcon, wep2.DisplayName, wep2.Description));

        foreach (LevelUpWeapon lvl in WeaponOptions)
        {
            GameObject go = Instantiate(WeaponSelectionWidgetPrefab, WeaponSelectionPanel);
            go.GetComponent<WeaponSelectionController>().Init(this, lvl);
        }

        LevelUpScreenCanvasGroup.alpha = 1;
        LevelUpScreenCanvasGroup.interactable = true;
        LevelUpScreenCanvasGroup.blocksRaycasts = true;
    }

    public void SelectWeapon(int selectedWeaponIndex)
    {
        PlayerCharacter.GetMyPlayer().SelectWeapon(PlayerCharacter.GetMyPlayer().CharacterType, selectedWeaponIndex);
        LevelUpScreenCanvasGroup.alpha = 0;
        LevelUpScreenCanvasGroup.interactable = false;
        LevelUpScreenCanvasGroup.blocksRaycasts = false;
    }
}
