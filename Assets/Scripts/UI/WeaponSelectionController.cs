using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSelectionController : MonoBehaviour
{
    public Image WeaponIcon;
    public Text WeaponName;
    public Text WeaponDescription;
    LevelUpWeapon data;
    LevelUpScreenController owner;


    public void Init(LevelUpScreenController controller, LevelUpWeapon Data)
    {
        data = Data;
        WeaponIcon.sprite = data.Icon;
        WeaponName.text = data.Name;
        WeaponDescription.text = data.Description;
        owner = controller;
    }

    public void SelectWeapon()
    {
        owner.SelectWeapon(data.PrefabIndex);
    }
}
