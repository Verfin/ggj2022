using UnityEngine;
using UnityEngine.Networking;

public class RotatingShooter : NetworkBehaviour
{

    public GameObject Weapon;
    public int NumberOfWeapons;
    private float attackTimer;

    [HideInInspector]
    private PlayerCharacter playerCharacter;

    public void BulletDestroyed()
    {
        NumberOfWeapons--;
    }

    // Start is called before the first frame update
    void Start()
    {
        attackTimer = Weapon.GetComponent<Weapon>().AttackSpeed;
        playerCharacter = GetComponentInParent<PlayerCharacter>();
        transform.SetParent(null);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = playerCharacter.transform.position;

        attackTimer -= Time.deltaTime;
        if (attackTimer <= 0 && NumberOfWeapons < Weapon.GetComponent<Weapon>().NumberOfProjectiles)
        {
            var targetTransform = playerCharacter.transform;
            if (targetTransform != null)
            {
                var go = Instantiate(Weapon, transform.position, transform.rotation, transform);
                go.GetComponent<Weapon>().Damage *= playerCharacter.DamageModifier;
                NetworkServer.Spawn(go);
                NumberOfWeapons++;
                attackTimer = go.GetComponent<Weapon>().AttackSpeed;
                go.GetComponent<Weapon>().SetShooter(playerCharacter.CharacterType);
                go.GetComponent<Weapon>().Initialize(targetTransform);
                go.GetComponent<RotatingWeapon>().Parent = this;
            }
        }
    }
}
