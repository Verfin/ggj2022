using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : Collectible
{

    public float PowerupStrength = 1.05f; // 5% speed increase
    public float Duration = 4;
    protected override void Collect(PlayerCharacter pc)
    {
        pc.AddBuff(new Buff(PowerupStrength, Duration, BuffType.Speed));
    }
}
