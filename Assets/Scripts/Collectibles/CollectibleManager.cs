using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Networking;

[System.Serializable]
public struct CollectiblePrefab
{
    public GameObject GameObject;
    public float Weight;

    public CollectiblePrefab(GameObject gameObject, float weight)
    {
        GameObject = gameObject;
        Weight = weight;
    }
}

public class CollectibleManager : MonoBehaviour
{
    public CollectiblePrefab Speed;
    public CollectiblePrefab Damage;
    public CollectiblePrefab Health;
    public CollectiblePrefab Exp_Small;
    public CollectiblePrefab Exp_Medium;
    public CollectiblePrefab Exp_Large;

    public float ChanceForPowerup = 0.7f;

    public static CollectibleManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private GameObject GetRandomCollectible()
    {
        List<CollectiblePrefab> prefabs = new List<CollectiblePrefab>() { Speed, Damage, Health, Exp_Small, Exp_Medium, Exp_Large };
        float total = 0;
        foreach (CollectiblePrefab cb in prefabs)
        {
            total += cb.Weight;
        }

        float random = Random.value * total;
        foreach (CollectiblePrefab cb in prefabs)
        {
            random -= cb.Weight;
            if (random <= 0)
            {
                return cb.GameObject;
            }
        }
        Debug.LogError("ohops");
        return null;
    }

    public void GenerateRandomPowerup(Vector3 position)
    {
        if (Random.Range(0f, 1f) < ChanceForPowerup)
        {
            // we have a wiener!
            GameObject powerup = GetRandomCollectible();
            GameObject gamo = Instantiate(powerup, position, Quaternion.identity);
            NetworkServer.Spawn(gamo);
        }
    }
}
