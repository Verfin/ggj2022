using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : Collectible
{

    public float PowerupStrength = 1.1f; // 10% damage increase
    public float Duration = 5;
    protected override void Collect(PlayerCharacter pc)
    {
        pc.AddBuff(new Buff(PowerupStrength, Duration, BuffType.Damage));
    }
}
