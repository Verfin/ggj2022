using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum BuffType {
    Damage,
    Speed,
    Health
}

public struct Buff
{
    public float Strength;
    public float Duration;
    public BuffType Type;

    public Buff(float strength, float duration, BuffType type)
    {
        Strength = strength;
        Duration = duration;
        Type = type;
    }
}

public abstract class Collectible : NetworkBehaviour
{
    public AudioClip PickupAudio;

    public void OnCollect(PlayerCharacter pc)
    {
        if (!isServer)
            return;

        Collect(pc);
        NetworkServer.Destroy(gameObject);
        Destroy(gameObject);
    }

    [ClientRpc]
    private void RpcCollected()
    {
        AudioManager.Instance.PlaySound(PickupAudio);
    }

    protected abstract void Collect(PlayerCharacter pc);

}
