using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : Collectible
{

    public float PowerupStrength = 0.1f; // heals 10% of max health
    protected override void Collect(PlayerCharacter pc)
    {
        pc.AddBuff(new Buff(PowerupStrength, 0, BuffType.Health));
    }
}
