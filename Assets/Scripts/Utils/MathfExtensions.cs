﻿using System.Collections.Generic;

namespace UnityEngine
{
    public static class MathfExtensions
    {
        /// <summary>
        /// Rotates the vector by an angle
        /// </summary>
        /// <param name="v">the vector</param>
        /// <param name="degrees">degrees to rotate</param>
        /// <returns>Returns rotated vector</returns>
        public static Vector2 Rotate(this Vector2 v, float degrees)
        {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

            float tx = v.x;
            float ty = v.y;
            v.x = (cos * tx) - (sin * ty);
            v.y = (sin * tx) + (cos * ty);
            return v;
        }

        public static Vector2 RandomPointInCircle(float radius)
        {
            float randDist = UnityEngine.Random.Range(0.0f, radius);
            float randAngle = UnityEngine.Random.Range(0.0f, 2.0f*Mathf.PI);

            return new Vector2( Mathf.Cos(randAngle)*randDist, Mathf.Sin(randAngle) * randDist);
        }

        public static void Shuffle<T>(List<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}