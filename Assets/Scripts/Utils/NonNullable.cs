
public struct NonNullable<T> where T : class
{
    private readonly T value;

    public NonNullable(T obj)
    {
        if (obj == null)
        {
            System.Diagnostics.Debugger.Break();
            throw new System.NullReferenceException("Non null can't be null!");
        }

        value = obj;
    }

    public T Get()
    {
        return value;
    }

    public static implicit operator T(NonNullable<T> wrapper)
    {
        return wrapper.value;
    }
}